-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-01-2019 a las 20:59:37
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `encuestaquaker`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE `encuesta` (
  `idencuesta` int(11) NOT NULL,
  `nombre_encuesta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`idencuesta`, `nombre_encuesta`) VALUES
(1, 'Encuesta de mantenimiento automotriz'),
(2, 'Encuesta de satisfacción'),
(3, 'Encuesta dsobre tú taller'),
(4, 'Encuesta sobre tipo de aceites');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(100) NOT NULL,
  `encuesta_idencuesta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id_pregunta`, `pregunta`, `encuesta_idencuesta`) VALUES
(1, '¿Qué aceite utilizarías en un motor V6 de 1998 con 125,579 km recorridos?', 1),
(2, '¿Cómo te sientes con el servicio de Quaker State?', 1),
(3, '¿Cuál es el producto que más utilizas?', 1),
(4, '¿Utilizas el material de Taller Academy?', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_opcion`
--

CREATE TABLE `preguntas_opcion` (
  `id_opcion` int(11) NOT NULL,
  `opcion` varchar(100) NOT NULL,
  `puntos` int(11) NOT NULL,
  `preguntas_id_pregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas_opcion`
--

INSERT INTO `preguntas_opcion` (`id_opcion`, `opcion`, `puntos`, `preguntas_id_pregunta`) VALUES
(1, 'Alto Kilometraje PRO', 5, 1),
(2, 'XTR-PRO', 4, 1),
(3, 'Ultimate Durability', 5, 1),
(4, 'Satisfecho', 5, 2),
(5, 'Neutral', 4, 2),
(6, 'Molesto', 3, 2),
(7, 'Alto Kilometraje', 5, 3),
(8, 'XTR Pro', 4, 3),
(9, 'Ultimate Durability', 3, 3),
(10, 'HD Heavy Duty', 3, 3),
(11, 'Si', 5, 4),
(12, 'No', 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regalo`
--

CREATE TABLE `regalo` (
  `idregalo` int(11) NOT NULL,
  `nombre_regalo` varchar(100) DEFAULT NULL,
  `descripcion_regalo` varchar(100) DEFAULT NULL,
  `imagen_regalo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `nombre_taller` varchar(100) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `direccion_taller` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `nombre_taller`, `ciudad`, `email`, `direccion_taller`, `password`, `puntaje`) VALUES
(1, 'Diego Antonio', 'ArmsTools', 'CDMX', 'ejemplo@hotmail.com', 'Zamora 39', 'password', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD PRIMARY KEY (`idencuesta`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`id_pregunta`),
  ADD KEY `fk_preguntas_encuesta1_idx` (`encuesta_idencuesta`);

--
-- Indices de la tabla `preguntas_opcion`
--
ALTER TABLE `preguntas_opcion`
  ADD PRIMARY KEY (`id_opcion`),
  ADD KEY `fk_preguntas_opcion_preguntas_idx` (`preguntas_id_pregunta`);

--
-- Indices de la tabla `regalo`
--
ALTER TABLE `regalo`
  ADD PRIMARY KEY (`idregalo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  MODIFY `idencuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `preguntas_opcion`
--
ALTER TABLE `preguntas_opcion`
  MODIFY `id_opcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD CONSTRAINT `fk_preguntas_encuesta1` FOREIGN KEY (`encuesta_idencuesta`) REFERENCES `encuesta` (`idencuesta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `preguntas_opcion`
--
ALTER TABLE `preguntas_opcion`
  ADD CONSTRAINT `fk_preguntas_opcion_preguntas` FOREIGN KEY (`preguntas_id_pregunta`) REFERENCES `preguntas` (`id_pregunta`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
