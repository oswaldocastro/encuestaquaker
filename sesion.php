<!DOCTYPE html>
<html>
<head>
	<title> Registro </title>
</head>
<script language="javascript">
			$(function () {
				$('#login').on('submit', function (e) {
					if (!e.isDefaultPrevented()) {
						var url = "php/login.php";
						var user = $('#emailUser').val();
						var pass = $('#passUser').val();
						$.ajax({
							type: "POST",
							url: url,
							dataType: 'json',
							data: {user:user,pass:pass},
							success: function (data) {
								if(data.success)
									window.location.href = "index.php";
								else{
									//swal("Error","Datos incorrectos","error");
									$('#emailUser').val('');
									$('#passUser').val('');
								}
							},
							error: function(data){
								alert('ocurrió un error');
							}
						});
						return false;
					}
				})
			});
		</script>
<body>
	<form method="POST" id="login">
		<!--div class="row justify-content-center">
			<div class="col-auto">
				<img src="img/logoQuaker.png" alt="Quaker State" class="img-fluid">
				<br><br>
				<h2 style="color: #01582b; font-weight: bold;">Aliado Quaker</h2>
			</div>
		</div-->
		<label> Iniciar Sesión </label>
		<div class="form-group">
				<input type="email" name="mail" id="emailUser" class ="form-control" placeholder="Correo Electrónico">
				<br>
				<input type="password" name="password" class ="form-control" id="passUser" placeholder="Contraseña">
				<br>
				<label> ¿Eres nuevo? Registrate <a href="#registro"> aquí </a></label>
				<div class="clearfix"></div>
				<button type ="submit" class="btn btn-primary"> Iniciar</button>
		</div>
	</form>
</body>
</html>