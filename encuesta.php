<?php session_start();?>
<script type="text/javascript">	
	function loadQuiz(id,container,encuesta){
		$.ajax({
			dataType: 'JSON',
			type: 'POST',
			url: 'php/quiz.php',
			data: {idencuesta:id},
			success: function (data){
				for(var i=0;i<data.length; i++){
					$(container).append("<legend>" +data[i].pregunta+" </legend>")
					for(var j=0;j<data[i].opciones.length;j++){
						console.log(data[i].idpregunta);
						$(container).append("<input type='radio' name='"+encuesta+"_pregunta_"+data[i].id_pregunta+"' value=''>"+ data[i].opciones[j].opcion+"<br>");
					}
				}
				$(container).append("<div align='center' id='"+encuesta+"'> <button  onclick='evalQuiz(\""+container+"\",\""+encuesta+"\")'> Enviar encuesta </button> </div>");
			},
			error: function (data){
				console.log("error");
				console.log(data);
			}
		});
	}
	function evalQuiz(container,encuesta){
		console.log(container);
		console.log(encuesta);
		if(encuesta == "encuesta_1"){
			$('#encuesta_1').remove();
			$(container).append("<div class='alert alert-success' role='alert' align='center'> Encuesta realizada</div>");
		}
		if(encuesta == "encuesta_2"){
			$('#encuesta_2').remove();
			$(container).append("<div class='alert alert-success' role='alert'  align='center'> Encuesta realizada</div>");
		}
		if(encuesta == "encuesta_3"){
			$('#encuesta_3').remove();
			$(container).append("<div class='alert alert-success' role='alert' align='center'> Encuesta realizada</div>");
		}
		if(encuesta == "encuesta_4"){
			$('#encuesta_4').remove();
			$(container).append("<div class='alert alert-success' role='alert' align='center'> Encuesta realizada</div>");
		}
	}

	$(document).ready(function () {
		loadQuiz(1,"#quiz1","encuesta_1");
		loadQuiz(1,"#quiz2","encuesta_2");
		loadQuiz(1,"#quiz3","encuesta_3");
		loadQuiz(1,"#quiz4","encuesta_4");

	});
</script>
<style>
	a h1,a h2,a h3,a h4,a h5,a h6{
		color: #fff;
	}
</style>
<body>
	<p>
  <div class="container">
  	<div class="row">
  		<div class="col-8 col-sm-8 align-middle" style="background-image: url('vendors/img/3.jpg')">
  			<a href="#" data-toggle="collapse" data-target="#encuesta1" aria-expanded="false" aria-controls="multiCollapseExample2"><h3> Encuesta de mantenimiento Automotriz</h3></a>
  		</div>
  		<div class="col-4 col-sm-4 title-quiz align-middle">
	  				25<br> puntos
	  		</div>
  	</div>
  </div>
</p>
<div class="collapse" id="encuesta1">
  <div class="card card-body">
    <div class ="container-fluid">
	    	<fieldset class = "form-group row">
	    		<div class="col-lg" >
		    			<div class="form-check" id="quiz1">
				          	
				        </div>
	    		</div>
	    	</fieldset>
    </div>
  </div>
</div>
 
 <p>
 <div class="container-fluid">
  	<div class="row">
  		<div class="col-8 col-sm-8   align-middle" style="background-image: url('vendors/img/3.jpg')">
  			<a href="#" data-toggle="collapse" data-target="#encuesta2" aria-expanded="false" aria-controls="multiCollapseExample2"><h3> Encuesta de satisfacción</h3></a>
  		</div>
  		<div class="col-4 col-sm-4 col-lg title-quiz align-middle">
	  				25<br> puntos
	  	</div>

  	</div>
  </div>
 	
 </p>

<div class="collapse" id="encuesta2">
  <div class="card card-body">
    <div class ="container-fluid">
    	
	    	<fieldset class = "form-group row">
	    			<div class="form-check">
			          <div class="form-check" id="quiz2">
				          	
				        </div>
			        </div>
	    	</fieldset>
    	
    </div>
  </div>
</div>

<div class="container">
  	<div class="row">
  		<div class="col-8 col-sm-8 align-middle" style="background-image: url('vendors/img/3.jpg')">
  			<a href="#" data-toggle="collapse" data-target="#encuesta3" aria-expanded="false" aria-controls="multiCollapseExample2" >
  			<h3> Encuesta sobre tu taller</h3>
  			</a>
  		</div>
  		<div class="col-4 col-sm-4 title-quiz align-middle">
	  				25<br> puntos
	  		</div>
  	</div>
  </div>
<div class="collapse" id="encuesta3">
  <div class="card card-body">
    <div class ="container-fluid">
    
	    	<fieldset class = "form-group row">
	    			<div class="form-check">
			          <div class="form-check" id="quiz3">
				          	
				        </div>
			        </div>
	    	</fieldset>
    	
    </div>
  </div>
</div>

<p>
 	<div class="container-fluid">
  	<div class="row">
  		<div class="col-8 col-sm-8 align-middle" style="background-image: url('vendors/img/3.jpg')">
  			<a href="#" data-toggle="collapse" data-target="#encuesta4" aria-expanded="false" aria-controls="multiCollapseExample2"><h3> Encuesta sobre tipos de aceite</h3></a>
  		</div>
  		<div class="col-4 col-sm-4 col-lg title-quiz align-middle">
	  				25<br> puntos
	  		</div>
  	</div>
  </div>
</p>

<div class="collapse" id="encuesta4">
  <div class="card card-body">
    <div class ="container-fluid">
    	
	    	<fieldset class = "form-group row">
	    			<div class="form-check">
			          <div class="form-check" id="quiz4">
				          	
				        </div>
			        </div>
	    	</fieldset>
    	
    </div>
  </div>
</div>
</body>


