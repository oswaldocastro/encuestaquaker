<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
</head>
<body>
	<form>
		<div class="form-group">
			<input type="text" name="nombre" id="nameUser" class ="form-control" placeholder="Nombre Completo">
			<br>
			<input type="text" name="direccionTaller" class ="form-control" id="tallerUser" placeholder="Dirección de tu taller">
			<br>
			<input type="text" name="ciudad" id="ciudadUser" class ="form-control" placeholder="Ciudad">
			<br>
			<input type="text" name="estado" class ="form-control" id="edoUser" placeholder="Estado">
			<br>
			<input type="email" name="email" id="emailUser" class ="form-control" placeholder="Correo Eletrónico">
			<br>
			<input type="password" name="pass1" class ="form-control" id="passUser1" placeholder="Contraseña">
			<br>
			<input type="password" name="pass2" class ="form-control" id="passUser2" placeholder="Confirmar Contraseña">
			<br>
			<button type ="submit" class="btn btn-primary"> Registrar</button>
		</div>
	</form>
</body>
</html>