<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>QuakerState </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script language="JavaScript" src="js/jquery-1.10.2.min.js"></script>
	<script language="JavaScript" src="js/sammy-0.5.4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){ });
        var ratPack = $.sammy(function() {
        this.element_selector = '#contenido'; 
        
      this.get('#inicio', function(context) {
        $("#contenido").load('encuesta.php');
          });    
        this.get('#sesion', function(context) {
        $("#contenido").load('sesion.php');
          });   
        this.get('#registro', function(context) {
        $("#contenido").load('registro.php');
          });         
        this.get('#puntaje', function(context) {
        $("#contenido").load('puntaje.php');
          });  
        this.get('#canjear', function(context) {
        $("#contenido").load('canjear.php');
          });      
        this.get('#preguntas', function(context) {
        $("#contenido").load('preguntas.php');
          });  
        this.get('#regalos', function(context) {
        $("#contenido").load('regalos.php');
          });       
        $(function() {
          ratPack.run('#inicio');
        });
        });
    </script>

</head>
<script  language="javascript">
	function login()
	{
		var usuario = $('#emailUser').val();
		var pass = $('#passUser').val();
		$.ajax({
			dataType: "JSON",
			type: "POST",
			url: "php/login.php",
			data: {usuario:usuario,pass:pass},
			success: function(data){
				if(data.success){
					window.location.reload();
				}
				else
				{
					console.log("Datos incorrectos");
				}
			} 
		});
	}

</script>
<body>
    <nav class="navbar  navbar-expand-lg navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
      <div class="container">
       <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <?php if(isset($_SESSION['nombre'])){ ?>
              <li class="nav-item">
                <a class="nav-link"> <?=$_SESSION['nombre']?> </a>
              </li>
            <?php }else{ ?>
              
            <?php } ?>

            <?php if(!isset($_SESSION['nombre'])){ ?>
            <li class="nav-item">
              <a class="nav-link" href="#sesion">Iniciar Sesión</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#registro">Registro</a>
            </li>
            <?php } else {?>
            <li class="nav-item">
              <a class="nav-link" href="#inicio">Actividades</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#puntaje"> Puntaje </a>
            </li>
            <li class="nav-item">
              <span class="badge badge-pill badge-light">100</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="php/logout.php">Salir</a>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </nav>
	

<div class="row justify-content-center">
  <div class="col-auto" style="margin-top: 30px;">
    <img src="img/logoQuaker.png" alt="Quaker State" class="img-fluid">
    <br><br>
    <h2 style="color: #01582b; font-weight: bold;">Aliado Quaker</h2>
  </div>
</div>
<div class="container my-5 py-5" id="contenido">
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Inicio de Sesión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label> Email </label>
        <input type="email" name="mail" class="form-control" id="emailUser" placeholder="example@domain.com" required>
        <label> Contraseña </label>
        <input type="password" name="pass" class="form-control" id="passUser" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="login()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>