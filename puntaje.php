<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title> Puntaje </title>
</head>
<script type="text/javascript">
	function canjear(){
		location.href="index.php#canjear"
	}
	function regalos(){
		location.href="index.php#regalos"
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="" style="text-align: center;">
					<p>PUNTAJE</p>
					<?php if(isset($_SESSION['nombre'])){ ?>
		                <p> <?=$_SESSION['puntaje']?> </p>
		            <?php }else{ ?>
		              
		            <?php } ?>
					<button type="button" onclick="canjear()"> Canjear</button>
					<button type="button" onclick="regalos()"> Regalos Obtenidos</button>
				</div>
				<br>
			</div>
			<div class="w-100"></div>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>
						Puntos Obtenidos
					</th>
					<th>
						Acción Realizada
					</th>
					<th>
						Fecha de relización
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						25 puntos
					</td>
					<td>
						Encuesta de mantenimiento automotriz
					</td>
					<td>
						11/12/18
					</td>
				</tr>
				<tr>
					<td>
						25 puntos
					</td>
					<td>
						Encuesta de satisfacción
					</td>
					<td>
						09/12/18
					</td>
				</tr>
				<tr>
					<td>
						25 puntos
					</td>
					<td>
						Encuesta sobre tu taller
					</td>
					<td>
						04/12/18
					</td>
				</tr>
				<tr>
					<td>
						25 puntos
					</td>
					<td>
						Encuesta sobre tipos de aceite
					</td>
					<td>
						14/01/2019
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</body>
</html>